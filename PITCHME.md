# PAMixxx

Portfolio Applied Machine Intelligence

AI hedging for smart portfolio design

#HSLIDE

### Flux Design

- Dispatcher: Manages Data Flow
- Stores: Handle State & Logic
- Views: Render Data via React

#HSLIDE

![Flux Explained](https://facebook.github.io/flux/img/flux-simple-f8-diagram-explained-1300w.png)
